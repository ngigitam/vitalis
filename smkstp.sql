-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 05, 2020 at 07:42 AM
-- Server version: 10.2.31-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u5554398_smkstp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `level` varchar(5) NOT NULL DEFAULT 'admin',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `level`, `username`, `password`) VALUES
(1, 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(1) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'dimatikan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id`, `status`) VALUES
(2, 'dihidupkan');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id` int(3) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `nope` varchar(15) NOT NULL,
  `judul` text NOT NULL,
  `isi` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `nama`, `nope`, `judul`, `isi`, `status`) VALUES
(9, 'Fadly', '081213070702', 'test1', 'hai', 2),
(10, 'rifai', '081213070702', 'test2', 'hai lagi', 2),
(14, 'dewi', '082176505590', 'biodata', 'harap di bantu', 2),
(16, 'Suci', '08217890999999', 'Peserta', 'Konfimasi', 2);

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE `peserta` (
  `id_peserta` int(4) UNSIGNED ZEROFILL NOT NULL,
  `nik` varchar(8) NOT NULL,
  `username` varchar(10) NOT NULL DEFAULT '',
  `password` varchar(10) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `nama_panggilan` varchar(20) NOT NULL,
  `level` varchar(8) NOT NULL DEFAULT 'peserta',
  `status` varchar(20) NOT NULL DEFAULT 'belum diverifikasi',
  `jenis_kelamin` varchar(12) NOT NULL,
  `agama` varchar(12) NOT NULL,
  `kewarganegaraan` varchar(20) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tanggal_lahir` int(2) UNSIGNED ZEROFILL NOT NULL,
  `bulan_lahir` int(2) UNSIGNED ZEROFILL NOT NULL,
  `tahun_lahir` int(4) UNSIGNED ZEROFILL NOT NULL,
  `usia` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `status_anak` varchar(15) NOT NULL,
  `anak_ke` int(3) NOT NULL,
  `jumlah_saudara` int(3) NOT NULL,
  `bahasa_seharihari` varchar(20) NOT NULL,
  `berat_badan` int(3) NOT NULL,
  `tinggi_badan` int(3) NOT NULL,
  `golongan_darah` varchar(3) NOT NULL,
  `penyakit` varchar(50) NOT NULL,
  `tinggal_pada` varchar(20) NOT NULL,
  `no_handphone` varchar(16) NOT NULL,
  `hobi` varchar(30) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `pendidikan_ayah` varchar(20) NOT NULL,
  `pendidikan_ibu` varchar(20) NOT NULL,
  `pekerjaan_ayah` varchar(20) NOT NULL,
  `pekerjaan_ibu` varchar(20) NOT NULL,
  `penghasilan` varchar(20) NOT NULL,
  `keterangan` varchar(20) NOT NULL DEFAULT 'TIDAK LULUS',
  `dokumen` text NOT NULL,
  `asal_sekolah` varchar(20) NOT NULL,
  `no_sttb` varchar(35) NOT NULL,
  `jenjang` varchar(15) NOT NULL,
  `unit` varchar(35) NOT NULL,
  `tempat_lahir_ayah` varchar(35) NOT NULL,
  `tgl_lahir_ayah` date NOT NULL,
  `agama_ayah` varchar(25) NOT NULL,
  `alamat_ayah` varchar(100) NOT NULL,
  `tempat_lahir_ibu` varchar(35) NOT NULL,
  `tgl_lahir_ibu` date NOT NULL,
  `agama_ibu` varchar(25) NOT NULL,
  `penghasilan_ibu` varchar(25) NOT NULL,
  `alamat_ibu` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id_peserta`, `nik`, `username`, `password`, `nama_lengkap`, `nama_panggilan`, `level`, `status`, `jenis_kelamin`, `agama`, `kewarganegaraan`, `tempat_lahir`, `tanggal_lahir`, `bulan_lahir`, `tahun_lahir`, `usia`, `alamat`, `status_anak`, `anak_ke`, `jumlah_saudara`, `bahasa_seharihari`, `berat_badan`, `tinggi_badan`, `golongan_darah`, `penyakit`, `tinggal_pada`, `no_handphone`, `hobi`, `nama_ayah`, `nama_ibu`, `pendidikan_ayah`, `pendidikan_ibu`, `pekerjaan_ayah`, `pekerjaan_ibu`, `penghasilan`, `keterangan`, `dokumen`, `asal_sekolah`, `no_sttb`, `jenjang`, `unit`, `tempat_lahir_ayah`, `tgl_lahir_ayah`, `agama_ayah`, `alamat_ayah`, `tempat_lahir_ibu`, `tgl_lahir_ibu`, `agama_ibu`, `penghasilan_ibu`, `alamat_ibu`) VALUES
(0020, '123', 'fadly', '123', 'fadly rifai', 'fadly', 'peserta', 'diverifikasi', 'Laki-Laki', '', '', '', 13, 06, 1995, '21 tahun 3 bulan 29 hari', 'Bekasi', '', 0, 0, '', 0, 0, '', '', 'orang tua', '', '', '', '', '', '', '', '', '', 'LULUS', 'aW1hZ2VzL3Nma19waG90b3Mvc2ZrX3Bob3Rvc18xMzQ3NTIxMTY3X0FJVWd6WEk0LmpwZw.jpg', '', '', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0044, 'admin', 'yani', 'admin', 'yani au', 'ya', 'peserta', 'belum diverifikasi', 'Perempuan', 'Islam', 'indonesia', 'muntok', 12, 12, 2001, '17 tahun 5 bulan 5 hari', 'jalan tanjung ular', 'Anak Kandung', 1, 1, 'melayu', 56, 64, 'ab', 'batuk', 'orang tua', '082176505590', 'mancing', 'andi', 'ani', 'smk', 'smk', 'guru', 'guru', '101', 'TIDAK LULUS', 'Kartu_keluarga7.png', '', '', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0045, 'admin', 'sandri', 'admin', 'sandrioi', 'om', 'peserta', 'belum diverifikasi', 'Laki-Laki', 'Kristen', '', '', 12, 12, 1995, '23 tahun 5 bulan 7 hari', 'jalan tanjung ular', 'Anak Kandung', 1, 2, 'indonesia', 68, 75, 'ab', 'patah hati', 'orang tua', '082176505590', 'mancing', 'ahew', 'moicin', 'smk', 'smk', 'pengusaha', 'guru', '168', 'TIDAK LULUS', 'Kartu_keluarga8.png', '', '', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0048, 'admin', 'rengga', 'admin', 'rengga saputra', 'putra', 'peserta', 'diverifikasi', 'Laki-Laki', 'Islam', 'indonesia', 'jakarta', 12, 00, 1995, '6735 tahun 11 bulan 11 hari', 'grogol', 'Anak Kandung', 3, 4, 'melayu', 52, 60, 'ab', 'demam berdarah', 'orang tua', '082176505590', 'mancing', 'budi', 'ani', 'sd', 'sd', 'nelayan', 'ibu rumah tangga', '559', 'LULUS', 'Kartu_keluarga11.png', '', '', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0055, 'admin', 'waturngui', 'admin', 'waturkhien', 'akhien', 'peserta', 'diverifikasi', 'Laki-Laki', 'Kristen', 'indonesia', 'jakarta', 28, 12, 1995, '23 tahun 4 bulan 27 hari', 'jl. tanjung gedung, grogol', 'Anak Kandung', 2, 3, 'indonesia', 70, 75, 'ab', 'maag', 'orang tua', '082176505591', 'mancing', 'budi', 'anisa', 'sd', 'sd', 'nelayan', 'ibu rumah tangga', '795', 'LULUS', 'Kartu_keluarga112.png', '', '', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0063, '12345678', 'Babon', '111213', 'Hago', 'Gogo', 'peserta', 'belum diverifikasi', '', '', '', '', 00, 00, 0000, '', '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', 'TIDAK LULUS', '', '', '', '', '', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0066, '321', 'a', '12345', 'aa', 'a', 'peserta', 'diverifikasi', '', '', '', '', 00, 00, 0000, '6736 tahun 12 bulan 1 hari', '', '', 0, 0, '', 0, 0, '', '', '', '021', '', '', '', '', '', '', '', '', 'LULUS', '', '', '', 'SD', 'Sekolah Santo Paulus I Jakarta', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0067, '33020120', 'RICO', 'rico123', 'HENRICO RADIANSYAH', 'RICO', 'peserta', 'belum diverifikasi', 'Laki-Laki', 'Islam', 'INDONESIA', 'BANYUMAS', 20, 08, 2007, '12 tahun 9 bulan 19 hari', 'Jl Sinyar Raya No. 5B, RT 14/RW 09 Duri Pulo Gambir Jakarta Pusat', 'Anak Kandung', 1, 0, 'BAHASA INDONESIA', 40, 155, '', '', 'orang tua', '081293698585', 'Main Bola', 'IVAN RADIANSYAH', 'RUMYATI', 'SMA', 'SMP', 'SWASTA', 'SWASTA', '3500000', 'TIDAK LULUS', 'IMG202006011214031.jpg', 'SDN KARANGGAYAM MUNJ', '', 'SMP', 'Sekolah Santo Paulus I Jakarta', 'PURWAKARTA', '1981-12-26', 'Islam', 'Jl Sinyar Raya No. 5B', 'BANYUMAS', '1982-06-05', 'Islam', '2500000', 'Jl Sinyar Raya No. 5B'),
(0068, '31710162', 'Melisha Ch', 'meli1234', 'Melisha Christiani', 'Melisha', 'peserta', 'belum diverifikasi', '', '', '', '', 00, 00, 0000, '', '', '', 0, 0, '', 0, 0, '', '', '', '087781682442', '', '', '', '', '', '', '', '', 'TIDAK LULUS', '', '', '', 'SMK', 'Sekolah Santo Paulus I Jakarta', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0069, '31710153', '01408.duri', 'jakpus91', 'Sripah sarkam', 'Sripah', 'peserta', 'belum diverifikasi', '', '', '', '', 00, 00, 0000, '', '', '', 0, 0, '', 0, 0, '', '', '', '089655574954', '', '', '', '', '', '', '', '', 'TIDAK LULUS', '', '', '', 'SMA', 'Sekolah Santo Paulus I Jakarta', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0070, '31710153', 'eugenesrip', 'eeesw382', 'Sripah sarkam', 'Sripah', 'peserta', 'belum diverifikasi', '', '', '', '', 00, 00, 0000, '', '', '', 0, 0, '', 0, 0, '', '', '', '089655574954', '', '', '', '', '', '', '', '', 'TIDAK LULUS', '', '', '', 'SMA', 'Sekolah Santo Paulus I Jakarta', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0071, '31730418', 'deffemell', 'Amelia12', 'Emelia Erhap', 'Amel', 'peserta', 'belum diverifikasi', '', '', '', '', 00, 00, 0000, '', '', '', 0, 0, '', 0, 0, '', '', '', '089503671683', '', '', '', '', '', '', '', '', 'TIDAK LULUS', '', '', '', 'SMK', 'Sekolah Santo Paulus I Jakarta', '', '0000-00-00', '', '', '', '0000-00-00', '', '', ''),
(0072, '32160241', 'Yunitatris', 'Yunita123', 'Yunita Tri Sianty', 'Nita', 'peserta', 'belum diverifikasi', '', '', '', '', 00, 00, 0000, '', '', '', 0, 0, '', 0, 0, '', '', '', '089505881503', '', '', '', '', '', '', '', '', 'TIDAK LULUS', '', '', '', 'SMK', 'Sekolah Santo Paulus I Jakarta', '', '0000-00-00', '', '', '', '0000-00-00', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `id_peserta` (`id_peserta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `peserta`
--
ALTER TABLE `peserta`
  MODIFY `id_peserta` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
