<div class="panel-heading">
    <h3 class="panel-title">Formulir Pendaftaran</h3>
  </div>
  <div class="panel-body">
  
   
  <h1>Data Siswa</h1>
<div class="progress" data-percent="">
												<div class="bar" style="width:100%;"></div>
											</div>

    
			<form class="form-horizontal" action="<?php echo site_url('admin/prosestambahpeserta');?>" method="post">
	<div class="control-group">
		<label class="control-label" for="form-field-2">NIK</label>
		<div class="controls">
			<input type="text" id="form-field-2" placeholder="" name="nik" value="" />						
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="nama_lengkap">Nama Lengkap</label>
		<div class="controls">
			<input type="text" id="nama_lengkap" placeholder="" name="nama_lengkap" value="" />						
		</div>
	</div>
  
	<div class="control-group">
		<label class="control-label" for="nama_panggilan">Nama Panggilan</label>
		<div class="controls">
			<input type="text" id="nama_panggilan" placeholder="" name="nama_panggilan" value="" />						
		</div>
	</div>
  
  <div class="control-group">
    <label for="jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
    <div class="controls">
      <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
			<option></option>
		  <option>Laki-Laki</option>
		  <option>Perempuan</option>
	</select>
    </div>
  </div>
    
	<div class="control-group">
		<label class="control-label" for="tempat_lahir">Tempat Lahir</label>
		<div class="controls">
			<input type="text" id="tempat_lahir" placeholder="" name="tempat_lahir" value="" />						
		</div>
	</div>
	
	<div class="control-group">
    <label class="control-label" for="tanggal_lahir">Tanggal Lahir</label>
		<div class="controls">
			<input type="text" id="tanggal_lahir" placeholder="" name="tanggal_lahir" value="" />	
			<input type="text" id="tanggal_lahir" placeholder="" name="bulan_lahir" value="" />
			<input type="text" id="tanggal_lahir" placeholder="" name="tahun_lahir" value="" />			
		</div>
	</div>

	
	<div class="control-group">
    <label for="agama" class="col-sm-2 control-label">Agama</label>
    <div class="controls">
      <select class="form-control" id="agama" name="agama">
			<option></option>
		  <option>Islam</option>
		  <option>Kristen</option>
		  <option>Katolik</option>
		  <option>Hindu</option>
		  <option>Buddha</option>
		   <option>Konghucu</option>
	</select>
    </div>
  </div>
  
	<div class="control-group">
		<label class="control-label" for="kewarganegaraan">Kewarganegaraan</label>
		<div class="controls">
			<input type="text" id="kewarganegaraan" placeholder="" name="kewarganegaraan" value="" />						
		</div>
	</div>
  
  <div class="control-group">
		<label class="control-label" for="anak_ke">Anak Ke</label>
		<div class="controls">
			<input type="number" id="anak_ke" placeholder="" name="anak_ke" value="" />						
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="jumlah_saudara">Jumlah Saudara</label>
		<div class="controls">
			<input type="number" id="jumlah_saudara" placeholder="" name="jumlah_saudara" value="" />						
		</div>
	</div>
  
  <div class="control-group">
		<label class="control-label" for="bahasa">Bahasa Sehari-hari</label>
		<div class="controls">
			<input type="text" id="bahasa" placeholder="" name="bahasa_seharihari" value="" />						
		</div>
	</div>
  
  <div class="control-group">
		<label class="control-label" for="berat_badan">Berat Badan</label>
		<div class="controls">
			<input type="text" id="berat_badan" placeholder="" name="berat_badan" value="" />						
		</div>
	</div>
  
  <div class="control-group">
		<label class="control-label" for="tinggi_badan">Tinggi Badan</label>
		<div class="controls">
			<input type="text" id="tinggi_badan" placeholder="" name="tinggi_badan" value="" />						
		</div>
	</div>
  
  <div class="control-group">
		<label class="control-label" for="golongan_darah">Golongan Darah</label>
		<div class="controls">
			<input type="text" id="golongan_darah" placeholder="" name="golongan_darah" value="" />						
		</div>
	</div>
  
  <div class="control-group">
		<label class="control-label" for="penyakit">Penyakit</label>
		<div class="controls">
			<input type="text" id="penyakit" placeholder="Penyakit yg pernah diderita, dapat diinput lebih dari 1" name="penyakit" value="" />						
		</div>
	</div>
	
  <div class="control-group">
    <label for="alamat" class="col-sm-2 control-label">Alamat</label>
    <div class="controls">
      <textarea row="5" class="form-control" id="alamat" name="alamat" value="" placeholder=""></textarea>
    </div>
  </div>
  
  <div class="control-group">
    <label for="status_anak" class="col-sm-2 control-label">Status Anak</label>
    <div class="controls">
      <select class="form-control" id="status_anak" name="status_anak">
			<option></option>
		  <option>Anak Kandung</option>
		  <option>Anak Tiri</option>
	</select>
    </div>
  </div>
  
	
  <div class="control-group">
    <label for="nope" class="col-sm-2 control-label">No Handphone</label>
    <div class="controls">
      <input type="number" class="form-control" id="nope" name="no_handphone" value="" placeholder="">
    </div>
	
	<label for="hobi" class="col-sm-3 control-label">Hobi</label>
	<div class="controls">
      <input type="text" class="form-control" id="hobi" name="hobi" value="" placeholder="">
    </div>
  </div>
  
  <div class="control-group">
    <label for="tinggal_pada" class="col-sm-2 control-label">Tinggal Pada</label>
    <div class="controls">
      <select class="form-control" id="tinggal_pada" name="tinggal_pada">
			<option></option>
		  <option>Orang Tua</option>
		  <option>Menampung</option>
		  <option>Asrama</option>
	</select>
    </div>
  </div>
      <div class="control-group">
    <label for="asal_sekolah" class="col-sm-2 control-label">Asal Sekolah</label>
   	<div class="controls">
      <input type="text" class="form-control" id="asal_sekolah" placeholder="Asal Sekolah" name="asal_sekolah" value="">
    </div>
  </div>
    <div class="control-group">
    <label for="inputPassword3" class="col-sm-2 control-label">No. STTB Asal Sekolah</label>
   <div class="controls">
      <input type="text" class="form-control" id="no_sttb" placeholder="No. STTB" name="no_sttb" value="">
    </div>
  </div>
  
   <div class="control-group">
    <label for="jenjang" class="col-sm-2 control-label">Pilihan Jenjang</label>
    <div class="controls">
      <select class="form-control" id="jenjang" name="jenjang">
			<option>Pilihan Jenjang</option>
		  <option>TK</option>
		  <option>SD</option>
		  <option>SMP</option>
		  <option>SMA</option>
		  <option>SMK</option>
	</select>
    </div>
  </div>
  
     <div class="control-group">
    <label for="unit" class="col-sm-2 control-label">Pilihan Unit</label>
   <div class="controls">
      <select class="form-control" id="unit" name="unit">
		    <option>Pilihan Unit</option>
		  <option>Sekolah Santo Paulus I Jakarta</option>
		  <option>Sekolah Santo Paulus II Jakarta</option>
		  <option>Sekolah Santo Paulus III Jakarta</option>
	</select>
    </div>
  </div>




<h1>Data Orang Tua</h1>
<div class="progress" data-percent="">
												<div class="bar" style="width:100%;"></div>
											</div>


    
	
  <div class="control-group">
    <label for="nama_ayah" class="col-sm-3 control-label">Nama Ayah</label>
    <div class="controls">
      <input type="text" class="form-control" id="nama_ayah" name="nama_ayah" value="" placeholder="">
    </div>
  </div>
  
  <div class="control-group">
    <label for="tempat_lahir_ayah" class="col-sm-3 control-label">Tempat Lahir Ayah</label>
    <div class="controls">
      <input type="text" class="form-control" id="tempat_lahir_ayah" name="tempat_lahir_ayah" value="" placeholder="Tempat Lahir Ayah">
    </div>
  </div>
  <div class="control-group">
    <label for="tgl_lahir_ayah" class="col-sm-3 control-label">Tangal Lahir Ayah</label>
    <div class="controls">
      <input type="date" class="form-control" id="tgl_lahir_ayah" name="tgl_lahir_ayah" value="" placeholder="Tangal Lahir Ayah">
    </div>
  </div>
 <div class="control-group">
    <label for="agama_ayah" class="col-sm-3 control-label">Agama Ayah</label>
    <div class="controls">
      <select class="form-control" id="agama_ayah" name="agama_ayah">
			<option>Pilihan Agama</option>
		  <option>Islam</option>
		  <option>Kristen</option>
		  <option>Katolik</option>
		  <option>Hindu</option>
		  <option>Buddha</option>
		  <option>Konghucu</option>
	</select>
    </div>
  </div>
    <div class="control-group">
    <label for="pendidikan_ayah" class="col-sm-3 control-label">Pendidikan Tertinggi Ayah</label>
    <div class="controls">
      <input type="text" class="form-control" id="pendidikan_ayah" name="pendidikan_ayah" value="" placeholder="SD / SMP / SMA / SMK / D3 / S1 / S2 / S3">
    </div>
  </div>
  
    <div class="control-group">
    <label for="pekerjaan_ayah" class="col-sm-3 control-label">Pekerjaan Ayah</label>
    <div class="controls">
      <input type="text" class="form-control" id="pekerjaan_ayah" name="pekerjaan_ayah" value="" placeholder="PNS/TNI/Peg. Swasta/WiraSwasta/Petani/Buruh">
    </div>
  </div>
  
    <div class="control-group">
    <label for="penghasilan" class="col-sm-3 control-label">penghasilan/Bulan Ayah</label>
    <div class="controls">
      <input type="number" class="form-control" id="penghasilan" name="penghasilan" value="" placeholder="Misal 2000000">
    </div>
  </div>
  
    <div class="control-group">
    <label for="alamat_ayah" class="col-sm-3 control-label">Alamat Ayah</label>
    <div class="controls">
      <textarea row="5" class="form-control" id="alamat_ayah" name="alamat_ayah" value="" placeholder="Alamat"></textarea>
    </div>
  </div>
  
  	<h3><b><u>Biodata Ibu Kandung</u></b></h3>
  
  <div class="control-group">
    <label for="nama_ibu" class="col-sm-3 control-label">Nama Ibu</label>
    <div class="controls">
      <input type="text" class="form-control" id="nama_ibu" name="nama_ibu" value="" placeholder="Nama Ibu">
    </div>
  </div>
  
  <div class="control-group">
    <label for="tempat_lahir_ibu" class="col-sm-3 control-label">Tempat Lahir Ibu</label>
    <div class="controls">
      <input type="text" class="form-control" id="tempat_lahir_ibu" name="tempat_lahir_ibu" value="" placeholder="Tempat Lahir Ibu">
    </div>
  </div>
  <div class="control-group">
    <label for="tgl_lahir_ibu" class="col-sm-3 control-label">Tangal Lahir Ibu</label>
    <div class="controls">
      <input type="date" class="form-control" id="tgl_lahir_ibu" name="tgl_lahir_ibu" value="" placeholder="Tangal Lahir Ibu">
    </div>
  </div>
 <div class="control-group">
    <label for="agama_ibu" class="col-sm-3 control-label">Agama Ibu</label>
    <div class="controls">
      <select class="form-control" id="agama_ibu" name="agama_ibu">
			<option>Pilihan Agama</option>
		  <option>Islam</option>
		  <option>Kristen</option>
		  <option>Katolik</option>
		  <option>Hindu</option>
		  <option>Buddha</option>
		  <option>Konghucu</option>
	</select>
    </div>
  </div>
  

  
  <div class="control-group">
    <label for="pendidikan_ibu" class="col-sm-3 control-label">Pendidikan Tertinggi Ibu</label>
    <div class="controls">
      <input type="text" class="form-control" id="pendidikan_ibu" name="pendidikan_ibu" value="" placeholder="SD / SMP / SMA / SMK / D3 / S1 / S2 / S3">
    </div>
  </div>
  

  
  <div class="control-group">
    <label for="pekerjaan_ibu" class="col-sm-3 control-label">Pekerjaan Ibu</label>
    <div class="controls">
      <input type="text" class="form-control" id="pekerjaan_ibu" name="pekerjaan_ibu" value="" placeholder="PNS/TNI/Peg. Swasta/WiraSwasta/Petani/Buruh">
    </div>
  </div>
  
    
    <div class="control-group">
    <label for="penghasilan_ibu" class="col-sm-3 control-label">penghasilan/Bulan Ibu</label>
    <div class="controls">
      <input type="number" class="form-control" id="penghasilan_ibu" name="penghasilan_ibu" value="" placeholder="Misal 2000000">
    </div>
  </div>
  
    <div class="control-group">
    <label for="alamat_ibu" class="col-sm-3 control-label">Alamat Ibu</label>
    <div class="controls">
      <textarea row="5" class="form-control" id="alamat_ibu" name="alamat_ibu" value="" placeholder="Alamat"></textarea>
    </div>
  </div>
  
<button type="submit" class="btn btn-primary btn-lg btn-block">Kirim</button>
</form>
	
	
	
  </div>