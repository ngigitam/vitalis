<div class="jumbotron">
  <h1>Selamat Datang!</h1>
  <p>Selamat Datang di <b>PPDB Online Sekolah Santo Paulus Jakarta.</b> Sebelum melakukan pendaftaran,
  sebaiknya anda memahami prosedur pendaftaran terlebih dahulu dihalaman 
  <b><?php echo anchor('ppdb/prosedur','Prosedur Pendaftaran');?></b></p>
  
  <p>Pastikan juga bahwa anda telah mengetahui <b>Jadwal PPDB</b> yang berada dihalaman 
  <b><?php echo anchor('ppdb/jadwal','Jadwal');?>.</b>
  
  Untuk mengetahui informasi terbaru mengenai <b>PPDB Online Sekolah Santo Paulus Jakarta</b> anda dapat melihatnya
  pada halaman <b><?php echo anchor('ppdb/pengumuman','Pengumuman');?></b></p>
  
  <p>Anda juga dapat melihat daftar peserta yang sudah melakukan pendaftaran di Sekolah Santo Paulus Jakarta melalui halaman <b><?php echo anchor('ppdb/daftarpeserta','Peserta');?></b></p>
  
  <p>Jika anda sudah memahami prosedur pendaftaran, silahkan klik tombol <b>Daftar</b> dibawah ini untuk melakukan <b>Pendaftaran!</b></p>
  
  <p><a class="btn btn-primary btn-lg" href="<?php echo site_url('ppdb/daftar');?>" role="button">Daftar</a></p>
</div>