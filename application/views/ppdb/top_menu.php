<nav class="navbar navbar-inverse">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Beranda</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url('ppdb/daftar');?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Pendaftaran</a></li>
        <li><a href="<?php echo site_url('ppdb/daftarpeserta');?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Peserta</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Informasi <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('ppdb/prosedur');?>"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Prosedur</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('ppdb/pengumuman');?>"><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> Pengumuman</a></li>
			<li><a href="<?php echo site_url('ppdb/jadwal');?>"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Jadwal</a></li>
          </ul>
        </li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Bantuan <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('ppdb/kontak');?>"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Kontak</a></li>
            
          </ul>
        </li>
      </ul>
<!--	  
	  <form class="navbar-form navbar-right" role="search" action="<?php echo site_url('ppdb/proses_login');?>" method="post">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo set_value('username')?>">
	<input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo set_value('password')?>">
  </div>
  <button type="submit" class="btn btn-default">Masuk</button>
</form>
  
	<div class="collapse navbar-collapse" id="myNavbar">
	-->
      <ul class="nav navbar-nav navbar-right">
        <li><a class="navbar-brand" data-toggle="modal" data-target="#myModal" href="#">Login</a></li>
      </ul>
    </div>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<script type="text/javascript">
	function cekform()
	{
		if (!$("#username").val())
		{
			alert('Maaf Username tidak boleh kosong');
			$("#username").focus();
			return false;
		}
		if (!$("#password").val())
		{
			alert('Maaf Password tidak boleh kosong');
			$("#password").focus();
			return false;
		}
	}
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modalh4"><h4><span class="glyphicon glyphicon-lock"></span> LOGIN</h4></div>
      </div>
      <div class="modal-body">
        <form role="form" action="<?php echo site_url('ppdb/proses_login');?>" method="post" onSubmit="return cekform();">
          <div class="form-group">
            <label for="psw"><span class="glyphicon glyphicon-user"></span> Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username">
          </div>
          <div class="form-group">
            <label for="usrname"><span class="glyphicon glyphicon-lock"></span> Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
		  
		  <?php
			$info = $this->session->flashdata('info');
			if (!empty($info))
			{
				echo $info;
			}
		  ?>
		  
          <button type="submit" class="btn btn-block">Login 
            <span class="glyphicon glyphicon-log-in"></span>
          </button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span> Cancel
        </button>
        <p>Need <a href="#">help?</a></p>
      </div>
    </div>
  </div>
</div>


































