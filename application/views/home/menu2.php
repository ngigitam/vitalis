<div id="menu" class="container-fluid text-center">
  <h2></h2>
  <h4></h4>
  <br>
  <div class="row">
    <!--div class="col-sm-4 col4">
      <a href="#"><span class="glyphicon glyphicon-globe logo-small"></span>
      <h4>BERITA</h4></a>
      <p>Berisi daftar berita/informasi terbaru</p>
    </div>
    <div class="col-sm-4 col4">
      <a href="#"><span class="glyphicon glyphicon-list-alt logo-small"></span>
      <h4>JADWAL</h4></a>
      <p>Cek jadwal pelajaran untuk masing-masing kelas</p>
    </div>
    <div class="col-sm-4 col4">
      <a href="#"><span class="glyphicon glyphicon-user logo-small"></span>
      <h4>MURID</h4></a>
      <p>Mencari data siswa/siswi</p>
    </div>
    </div>
    <br><br-->

    <div class="col-sm-4 col4">
      <a href="http://santopaulusjakarta.sch.id/"><span class="glyphicon glyphicon-globe logo-small"></span>
      <h4>WEBSITE</h4></a>
      <p>Website Sekolah Santo Paulus</p>
    </div>
    <div class="col-sm-4 col4">
      <a href="<?php echo site_url('ppdb');?>"><span class="glyphicon glyphicon-list-alt logo-small"></span>
      <h4>PPDB</h4></a>
      <p>Pendaftaran Peserta Didik Baru</p>
    </div>
    
    <div class="col-sm-4 col4">
      <a href="login#" data-toggle="modal" data-target="#myModal" ><span class="glyphicon glyphicon-user logo-small"></span>
      <h4>LOGIN</h4></a>
      <p>Untuk mengakses login</p>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modalh4"><h4><span class="glyphicon glyphicon-lock"></span> LOGIN</h4></div>
      </div>
      <div class="modal-body">
        <form role="form" action="<?php echo site_url('admin/login');?>" method="post" onSubmit="return cekform();">
          <div class="form-group">
            <label for="psw"><span class="glyphicon glyphicon-user"></span> Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username">
          </div>
          <div class="form-group">
            <label for="usrname"><span class="glyphicon glyphicon-lock"></span> Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
		  
		  <?php
			$info = $this->session->flashdata('info');
			if (!empty($info))
			{
				echo $info;
			}
		  ?>
		  
          <button type="submit" class="btn btn-block">Login 
            <span class="glyphicon glyphicon-log-in"></span>
          </button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span> Cancel
        </button>
        <p>Need <a href="#">help?</a></p>
      </div>
    </div>
  </div>
</div>